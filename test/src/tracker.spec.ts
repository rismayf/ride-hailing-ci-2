import { random } from "faker";
import { expect } from "chai";
import { dbQuery, syncDB } from "./utils/db";
import { get } from "request-promise-native";

const TRACKER_HOST = process.env['TRACKER_HOST'] || 'localhost';
const TRACKER_PORT = process.env['TRACKER_PORT'] || '3000'

describe("Tracker Server", function() {
    this.timeout(50000);
    
    before(function (done) {
        setTimeout(done, 15000)
    });

    beforeEach(async function() {
        await syncDB();
        //reset db
        await dbQuery({type: "remove", table: "track_events"});
        //feed data
        this.tracks = (await dbQuery({
            type: "insert",
            table: "track_events",
            values: {
                "rider_id": 4,
                "north" : random.number({min:1, max:20}),
                "west": random.number({min:1, max:20}),
                "east" : random.number({min:1, max:20}),
                "south": random.number({min:1, max:20}),
                "createdAt" : new Date(),
                "updatedAt" : new Date()
            },
            returning: [
                "rider_id",
                "north",
                "west",
                "east",
                "south",
                "createdAt",
                "updatedAt"
            ]
        }))[0][0]; 
    });
    
    describe("Movement", function() {
        it("Should Return Data of Rider", async function() {
            const response = await get(
                `http://${TRACKER_HOST}:${TRACKER_PORT}/movement/4`,
                {json : true}
            );
            expect(response.ok).to.be.true;
            // expect(response.logs).to.be.deep.equal([{
            //     north: this.tracks.returning["north"]
            // }]);
            console.log(response);
        });
    });
});